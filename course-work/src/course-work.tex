\documentclass[12pt, a4paper]{article}

\usepackage{xltxtra} % XeLaTex

\setromanfont{Liberation Sans} %Fonts setup
\setsansfont{Liberation Sans}
\setmonofont{DejaVu Sans Mono}

\usepackage{geometry}
\geometry{verbose,a4paper,tmargin=2cm,bmargin=2cm,lmargin=3.5cm,rmargin=1cm}

\usepackage{polyglossia} %Languages
\setmainlanguage{russian}
\setotherlanguage{english}

\PolyglossiaSetup{russian}{indentfirst=true} %Indentation
\parindent = 1cm

\usepackage{setspace}
\onehalfspacing
\hyphenpenalty=10000
\tolerance=1
\emergencystretch=1000

\usepackage{titlesec} %Настройка заголовков
\newcommand{\sectionbreak}{\clearpage}
%\newcommand{\section}[1]{\section*{#1}\addcontentsline{toc}{section}{#1}}
%\newcommand{\anonsubsection}[1]{\subsection*{#1}\addcontentsline{toc}{subsection}{#1}}

\usepackage{enumitem} %Itemize customization

\usepackage[figurename=Рис.]{newfloat} %Floating environments
\usepackage{graphicx} %Images
\usepackage{pdfpages}
\DeclareGraphicsExtensions{.pdf,.png}
\graphicspath{{images/pdf/}{images/png/}}

\usepackage[outputdir=\detokenize{../out/}, newfloat=true]{minted}
\setminted[sql]{
linenos=true,
breaklines=true,
encoding=utf8,
frame=single,
tabsize = 3
}
\makeatletter
\AtBeginEnvironment{minted}{\dontdofcolorbox}
\newcommand{\dontdofcolorbox{\renewcommand\fcolorbox[4][]{##4}}}{\xpatchcmd{\inputminted}{\minted@fvset}{\minted@fvset\dontdofcolorbox}{}{}}
\makeatother %Fix red box around cyrillic symbols in listings

\SetupFloatingEnvironment{listing}{name=Листинг}
\addto\captionsrussian{\renewcommand{\contentsname}{Оглавление}}

\usepackage[doi=false, url=false, sorting=ynt]{biblatex}
\usepackage{amsmath}
\addbibresource{sources.bib}

\begin{document}

	\includepdf{title-page}

	\tableofcontents

	\begin{section}{Приближение функций полиномами наилучшего равномерного приближения}

		\begin{subsection}{Постановка задачи}
			\textbf{Вариант 17.}
			Рассмотрим следующую функцию:
			\begin{equation}
				f(t) = (3t+5)^{-2}
			\end{equation}
			\begin{enumerate}
				\item На отрезке \([-1;1]\) построим многочлен первой степени наилучшего равномерного приближения \(P_N(t)\).
				\item Для заданной функции на отрезке \([-1;1]\) построим интерполяционной многочлен первой степени \(P_R(t)\) по узлам \(t=-1\), \(t=1\).
				\item Для узлов--корней многочлена Чебышева второй степени построим интерполяционный многочлен 1-й степени \(P_T(t)\).
				\item Построим графики полученных функций.
				\item Вычислим абсолютные погрешности полученных значений.
			\end{enumerate}
		\end{subsection}

		\begin{subsection}{Теоретическая часть}
			\textbf{Полиномы наилучшего равномерного приближения.}
			Теория аппроксимации (теория приближений) --- это раздел математики, изучающий вопрос о возможности приближенного представления одних математических объектов другими, как правило, более простой природы, а также вопросы об оценках вносимой при этом погрешности. Одна из основных задач теории аппроксимации заключается в том, чтобы для фиксированного элемента x найти в каком-то множестве ближайший элемент.
			Элемент \(\widetilde{x} \in \widetilde{X}\) называется элементом наилучшего приближения для \(x\) в подпространстве X, если
			\begin{equation}
				\parallel x-\widetilde{x} \parallel = \max_{y \in \widetilde{X}} \parallel x-y \parallel
				\label{best_approx_condition}
			\end{equation}

			в пространстве непрерывных функций \(C[a;b]\) норма определяется следующим образом:
			\begin{equation}
				\parallel x(t) \parallel = \max_{t \in [a;b]} x(t)
			\end{equation}
			тогда~\ref{best_approx_condition} примет вид
			\begin{equation}
				\( |x(t) - \widetilde{x}(t)| = (|x(t) - y(t)|) \)
			\end{equation}
			а приближение получит название равномерного, иными словами, элементом наилучшего равномерного приближения функции \(x(t) \in С[a;b] \) является функция \(\widetilde{x}(t)\) такая, что наибольшее абсолютное отклонение \(\widetilde{x}(t)\) от \(x(t)\) на отрезке \( [a;b] \) минимально.

			\textbf{Интерполяционные полиномы.}
			Интерполяцией называют такую разновидность аппроксимации, при которой кривая построенной функции проходит точно через имеющиеся точки данных.

			Существует также задача, заключающаяся в аппроксимации какой-либо сложной функции другой, более простой функцией.
			Если для исследуемой функции не удается достигнуть достаточной производительности вычислений, можно попробовать вычислить её значение в нескольких точках и построить по ним более простую функцию.
			При достаточной точности интерполяции, для некоторых классов задач будет допустимо использование такой аппроксимирующей функции вместо исходной.

			Существует большое множество способов интерполяции: начиная от базовых методов, например линейной интерполяции, заканчивая сплайнами и бикубической интерполяцией.

		\end{subsection}

		\pagebreak
		\begin{subsection}{Реализация на практике}
			Построим график заданной функции \(f(t)\):

			\noindent
			\begin{figure}[h]
				\includegraphics[width=\linewidth]{original-plot}
				\caption{График заданной функции}
			\end{figure}

			Построем приближение этой функции многочленом \(PN_1(x) = A_0 + A_1 x\), где \(A_0\) и \(A_1\) - коэффициенты многочлена \(PN_1(x)\).
			Все приведенные вычисления проведены в среде Wolfram Mathematica.
			Реализуем функцию, выполняющую построение искомого многочлена:

			\begin{figure}[h]
				\includegraphics{getPN1}
				\caption{Вычисление многочлена наилучшего равномерного приближения}
			\end{figure}

			\pagebreak

			Построим график полученного многочлена:

			\noindent
			\begin{figure}[h]
				\includegraphics[width=\linewidth]{pn1-plot}
				\caption{График многочлена первой степени наилучшего равномерного приближения}
			\end{figure}

			Для заданной функции на отрезке \([-1;1]\) построим интерполяционной многочлен первой степени \(P_R(t)\) по узлам \(t=-1\), \(t=1\) и многочлен \(P_T(t)\) для узлов--корней многочлена Чебышева второй степени.

			Для этого зададим функцию построения интерполяционного многочлена по двум узлам и функцию получения узлов Чебышева:
			\begin{figure}[h]
				\includegraphics{interpolation-polynoms}
				\caption{Интерполяционные многочлены по двум узлам}
			\end{figure}

			\pagebreak

			Построи график исходной функции и трех полученных многочленов:

			\noindent
			\begin{figure}[h]
				\includegraphics[width=\linewidth]{interpolation-polynoms-plots}
				\caption{\(PN_1(t), PT_1(t) и PR_1(t)\)}
			\end{figure}

		\end{subsection}

		\pagebreak
		\begin{subsection}{Оценка погрешностей и выводы}

			Оценим погрешности:

			\noindent
			\includegraphics[width=250pt]{part-1-errors}

			Интерполяционный полином первой степени по узлам \(t=-1\) и \(t=1\) приблизил функцию лучше всего в этих точках, так как строился по ним.
			Однако значение погрешности в точке \(c\) значительно выше, чем в случае с другими методами.
			Интерполяционный полином, построенный методом равномерного приближения, даѐт в каждом случае меньшую погрешность в рассмотренных точках, чем максимальная погрешность каждого из остальных методов.
		\end{subsection}

	\end{section}

	\begin{section}{Ряд Фурье. Интерполяционный ряд Фурье.}

		\begin{subsection}{Постановка задачи}
			Для функции \(\sin(3t+4)t^4\)
			\begin{enumerate}
				\item На отрезке \([-\pi; \pi]\) построить ряд Фурье. Оценить погрешность приближения частичным рядом с 5 членами.
				\item На отрезке \([-\pi; \pi]\) построить интерполяционный ряд Фурье по \(n=10\) точкам. Оценить погрешность приближения.
			\end{enumerate}
		\end{subsection}

		\clearpage
		\begin{subsection}{Построение ряда Фурье}
			Построим ряд Фурье в пространстве \(L^2(-\pi;\pi)\) по ортонормированной системе многочленов Чебышева I рода.

			Введем необходимые функции для работы с рядами Фурье в данном пространстве:

			\begin{figure}[h]
				\includegraphics{fourier-funcs}
				\caption{Функции для работы с рядами Фурье в пространстве \(L^2\)}
			\end{figure}

			\clearpage

			Выпишем несколько членов ортонормированного базиса, составленного из многочленов Чебышева:

			\begin{figure}[h]
				\includegraphics{fourier-basis}
				\caption{Ортонормироанная система многочленов Чебышева}
			\end{figure}

			Построим частичную сумму ряда Фурье шестого порядка по построенной системе многочленов и построим соответствующий график.
			Экспериментальныым путем было выявлено, что заданного по условию взятия первых пяти членов недостаточно для получения более или менее близкой аппроксимации, поэтому были взяты первые шесть членов.

			\noindent
			\begin{figure}[h]
				\includegraphics[width=\linewidth]{fourier-plot}
				\caption{Частичная сумма ряда Фурье для заданной функции}
			\end{figure}

			\clearpage

			Оценим погрешность аппроксимации.
			Оценим качество равномерной аппроксимации на \([-\pi];\pi]]\), а также модули разностей значений в пробных точках t=-1.5, t=0.5:
			\begin{figure}[h]
				\includegraphics{fourier-errors}
				\caption{Погрешности аппроксимации рядом Фурье}
			\end{figure}

		\end{subsection}

		\clearpage

		\begin{subsection}{Построение интерполяционного ряда Фурье}
			Построим интерполяционный ряд Фурье следующего вида:

			\begin{equation}
				F_n(t)=a_0+a_1 \cos(t) + b_1 \sin(t) + \dots + a_n \cos(n x)+b_n \sin(n x),
			\end{equation}

			где \(a_m = \frac{1}{N} \sum{j=0}^{N-1} y_j \cos(m x_j)\) и \(b_m = \frac{1}{N} \sum{j=0}^{N-1} y_j \sin(m x_j)\).

			Проведем построение по равномерно распределенным узлам и узлам Чебышева.
			Рассчитаем пары узловых точек для каждого метода:

			\begin{figure}[h]
				\includegraphics{xyPairs}
				\caption{Узлы для интерполяции рядами Фурье}
			\end{figure}

			\clearpage

			Рассчитаем соответствующие полиномы:

			\begin{figure}[h]
				\includegraphics{fourier-interpolational-polynoms}
				\caption{Тригонометрические интерполяционные полиномы}
			\end{figure}

			\clearpage

			Построим их графики:

			\begin{figure}[h]
				\includegraphics[width=\linewidth]{uniform-fn10}
				\caption{\(F_{10}\) по равномерным узлам}
			\end{figure}

			\begin{figure}[h]
				\includegraphics[width=\linewidth]{chebyshev-fn10}
				\caption{\(F_{10}\) по узлам Чебышева}
			\end{figure}


			\clearpage

			Оценим погрешности:

			\vspace{2mm}
			\includegraphics[width=220pt]{fourier-interpolational-polynoms-errors}
			\vspace{2mm}

			Можно заметить, что в данном случае тригонометрическая интерполяция показала себя лучше построенной ранее аппроксимации частичным рядом Фурье.
			Также видно, что построение по узлам Чебышева позволило получить лучшую равномерную аппроксимацию, чем при равномерном разбиении узлов.
		\end{subsection}

	\end{section}

	\begin{section}{Выводы}
		Были получены навыки решения задач на языке SQL и лучшее понимание оптимальных сфер его применения.
	\end{section}

	\printbibliography[title = Список использованных источников]
\end{document}
